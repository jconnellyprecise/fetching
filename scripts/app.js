/**
 * Fetch a community available rest api resorce using GET.
 */
function simpleGet(element) {
    console.log('simpleGet start');

    fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then(response => {
        console.dir(response);
        if (response.ok) {
        return response.json();
        }
        throw response;
    })
    .then(data => {
        console.dir(data);
        element.innerHTML = 'it works!!!';
    })
    .catch(error => {
        console.dir(error);
        element.innerHTML = 'Error: ' + error?.message;
    }); 

    console.log('simpleGet done');     
}


/**
 * Fetch a community available rest api resorce using POST.
 */
function simplePost(element) {
    console.log('simplePost start');

    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
        title: 'foo',
        body: 'bar',
        userId: 1,
        }),
        headers: {
        'Content-type': 'application/json; charset=UTF-8',
        },
    })
    .then(response => {
        console.dir(response);
        if (response.ok) {
        return response.json();
        }
        throw response;
    })
    .then(data => {
        console.dir(data);
        element.innerHTML = 'it works!!!';
    })
    .catch(error => {
        console.dir(error);
        element.innerHTML = 'Error: ' + error?.message;
    }); 

    console.log('simplePost done');     
}


/**
 * Fetch precise api running locally.
 */
function preciseLocalPost(element) {
    console.log('preciseLocalPost start');

    let url = 'http://localhost:57194/api/v202007/companies/182/Reports/RawDataByReceivedTime';

    let postBody = {
        StartDateTime: '2021-12-06T07:00:00.000Z',
        EndDateTime: '2021-12-06T12:00:00.000Z',
        AssetIds: [66883]
    }

    let corsRequestHeader = {
        'Accept': '*/*',
        'Content-Type': 'application/json; charset=utf-8',
        'Origin': 'http://127.0.0.1:5500',
        'User-Name': 'jconnelly@precisemrm.com',
        'Api-Key': 'dfda23878abdc083a0714ccad6811f0216b4ff8b213200d7331319a759d80df9'
    };

    let options = {
        method: 'POST',
        mode: 'cors',
        cache: 'force-cache',
        credentials: 'include',
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(postBody), 
        headers: corsRequestHeader
    };

    try {
        fetch(url, options)
        .then(response => {
            if (response.ok) {  
                return Promise.resolve(response);
            } else {
                throw new Error(response.statusText);
            }
        })
        .then(async data => {
            element.innerHTML = "";
            const content = await data.json();
            for (let i = 0; i < content.FleetRawData.length; i++) {
                const fleetData = content.FleetRawData[i];
                if (fleetData !== null) {
                    for (let j = 0; j < fleetData.AssetRawData.length; j++) {
                        const assetData = fleetData.AssetRawData[j];
                        for (let k = 0; k < assetData.Positions.length; k++) {
                            const position = assetData.Positions[k];
                            if (position !== null && position.Location !== null) {
                                if (!(position.Location.Latitude === 0 && position.Location.Longitude === 0)) {
                                    element.innerHTML += '\n' + position.Location.Latitude + ',' + position.Location.Longitude + ';';
                                }
                            }
                        }                        
                    }
                }
            }
        })
        .catch(rejected => {
            element.innerHTML = 'Error: ' + rejected.message;       
        });
    }
    catch (ex) {
        element.innerHTML = error.message;
    }

    console.log('preciseLocalPost done');     
}   


/**
 * Fetch precise api running externally in AWS.
 * NOTE: THIS IS NOT WORKING SINCE OUR API IN PROD DOES NOT SUPPORT
 * CROSS-SITE ORIGINS, ALTHOUGH I MADE THESE CHANGES TO MY LOCAL API.
 */
function preciseExternalPost(element) {
    console.log('preciseExternalPost start');

    let url = 'https://api-dtn.precisemrm.com/api/v202007/companies/182/Reports/RawDataByReceivedTime';

    let postBody = {
        StartDateTime: '2021-12-06T07:00:00.000Z',
        EndDateTime: '2021-12-06T23:00:00.000Z',
        AssetIds: [66883]
    }

    let header = {
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        //'Access-Control-Allow-Origin': '*', // tells browsers to allow requesting code from any origin to access the resource. Attempting to use the wildcard with credentials results in an error.
        //'Content-type': 'text/plain',
        'Content-type': 'application/json; charset=utf-8',
        'User-Name': 'jconnelly@precisemrm.com',
        'Api-Key': 'dfda23878abdc083a0714ccad6811f0216b4ff8b213200d7331319a759d80df9'
    };

    let options = {
        method: 'POST',
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'default', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'include', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url 
        body: JSON.stringify(postBody), 
        headers: header
    };

    try {
        fetch(url, options)
        .then(response => {
        if (response.ok) {
            return Promise.resolve(response);
        } else {
            console.dir(response);
            throw new Error(response.statusText);
        }
        })
        .then(data => {
        console.dir(data);
        const content = data.json();
        element.innerHTML = 'success!!!';
        })
        .catch(rejected => {
        console.dir(rejected);
        element.innerHTML = 'Error: ' + rejected.message;       
        });
    }
    catch (ex) {
        console.log('Failed to load fetch resource: net::ERR_FAILED');
        console.dir(error);
        element.innerHTML = error.message;
    }

    console.log('preciseExternalPost done');     
}